'use strict';

/**
 * @ngdoc overview
 * @name valtechTestApp
 * @description
 * # valtechTestApp
 *
 * Main module of the application.
 */
angular
  .module('valtechTestApp', [
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ngMap'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
