'use strict';

/**
 * @ngdoc function
 * @name valtechTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the valtechTestApp
 */
angular.module('valtechTestApp')
  .controller('MainCtrl', function ($http, $compile, $scope, $rootScope, NgMap) {

    var main = this;
    NgMap.getMap().then(function(map) {
      main.map = map;
      main.height = 200;
    });

    main.showHome = function(id) {
      var getId = this.id;
      $scope.infoWindow = $scope.markers[getId];
      main.map.showInfoWindow('info', this);
    };

    function pageLoad() {
      $http.get("/data/data1.json")
      .then(function(response) {
        $rootScope.markers = response.data;
      });
    }

    $rootScope.markers = pageLoad();

    $scope.json1 = function(){
      pageLoad();
    };
    $scope.json2 = function(){
      $http.get("/data/data2.json")
      .then(function(response) {
        $rootScope.markers = response.data;
      });
    };
    $scope.json3 = function(){
      $http.get("/data/data3.json")
      .then(function(response) {
        $rootScope.markers = response.data;
      });
    };
  });
