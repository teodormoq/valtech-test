'use strict';

describe('Filter: maxPrice', function () {

  // load the filter's module
  beforeEach(module('valtechTestApp'));

  // initialize a new instance of the filter before each test
  var maxPrice;
  beforeEach(inject(function ($filter) {
    maxPrice = $filter('maxPrice');
  }));

  it('should return the input prefixed with "maxPrice filter:"', function () {
    var text = 'angularjs';
    expect(maxPrice(text)).toBe('maxPrice filter: ' + text);
  });

});
